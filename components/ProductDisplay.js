app.component('product-display',{
  props: {
    premium: {
      type: Boolean,
      required: true
    }
  },
  template:
  `<div class="product-display">
    <div class="product-container">
      <div class="product-image">
          <img :src="image">
      </div>
      <div class="product-info">
        <h1>{{ title }}</h1>
        <h3>Details</h3>
        <ul>
          <li v-for="detail in details">{{ detail }}</li>
        </ul>
        <p><strong>Shipping:</strong> {{ shipping }}</p>
        <p v-if = "inStock">In Stock</p>
        <p v-else>Out of Stock</p>
        <div
          class="color-circle" v-for="(variant, index) in variants" :key="variant.id" @mouseover="updateVariant(index)" :style="{ backgroundColor: variant.color }">
        </div>
        <button
          type="button" @click="addToCart" :class="{ disabledButton: !inStock }"  :disabled="!inStock">
          Add to Cart
        </button>
      </div>
    </div>
    <review-list :reviews="reviews"></review-list>
    <review-form @review-submitted = "addReview"></review-form>
  </div>`,
  data(){
    return{
      product: 'Socks',
      brand: 'Vue Mastery',
      selectedVariant: 0,
      details: ['50% cotton', '30% wool', '20% polyester'],
      variants: [
        { id: 2234, color: 'Blue', image: 'assets/images/blue_sock.jpg', quantity: 50},
        { id: 2235, color: 'Green', image: 'assets/images/green_sock.jpg', quantity: 0}
      ],
      reviews:[]
    }
  },
  methods: {
    addToCart(){
      this.$emit('add-to-cart', this.variants[this.selectedVariant].id)
    },
    updateVariant(index) {
      this.selectedVariant = index
    },
    addReview(review) {
      this.reviews.push(review)
    }
  },
  computed: {
    title() {
      return this.brand + ' ' + this.product
    },
    image() {
      return this.variants[this.selectedVariant].image
    },
    inStock() {
      return this.variants[this.selectedVariant].quantity
    },
    shipping() {
      if (this.premium) {
        return 'Free'
      }
        return 50
    }
  }
})
